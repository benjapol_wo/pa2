package ku.util;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * An iterator that will iterate over the received array.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.23
 *
 * @param <E>
 *            - data type of elements in the array.
 */
public class ArrayIterator<E> implements Iterator<E> {
	/** Array to be iterate on. */
	private E[] array;
	/** Cursor pointing to an array index */
	private int cursor;

	/**
	 * Construct the ArrayIterator and initialize the cursor to 0.
	 * 
	 * @param array
	 *            - an array to be iterate on.
	 */
	public ArrayIterator(E[] array) {
		this.array = array;
		this.cursor = 0;
	}

	@Override
	public boolean hasNext() {
		for (int i = cursor; i < array.length; i++) {
			if (array[i] != null) {
				return true;
			}
		}
		return false;
	}

	@Override
	public E next() {
		if (!hasNext()) {
			throw new NoSuchElementException();
		}
		if (array[cursor] != null) {
			return array[cursor++];
		}
		if (array[cursor] == null && cursor < array.length) {
			cursor++;
			return next();
		}
		return null;
	}

	/**
	 * Remove the last element returned by next().
	 */
	public void remove() {
		for (int i = cursor - 1; i >= 0; i--) {
			if (array[i] != null) {
				array[i] = null;
				break;
			}
		}
	}
}
