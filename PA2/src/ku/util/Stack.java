package ku.util;

import java.util.EmptyStackException;

/**
 * A standard last-in first-out (LIFO) stack.
 * 
 * @author Benjapol Worakan 5710546577
 * @version 15.2.23
 *
 * @param <E>
 *            - data type of elements in the stack.
 */
public class Stack<E> {
	/** Elements in the stack */
	private E[] elements;

	/**
	 * Create a new Stack with a fixed capacity.
	 * 
	 * @param capacity
	 *            - number of element(s) that the Stack will be able to store.
	 */
	@SuppressWarnings("unchecked")
	public Stack(int capacity) {
		elements = (E[]) new Object[capacity];
	}

	/**
	 * Get the capacity of the Stack.
	 * 
	 * @return capacity of the Stack (number of element(s) that it can store).
	 */
	public int capacity() {
		return elements.length;
	}

	/**
	 * Check if the Stack is empty.
	 * 
	 * @return true if the Stack is empty, false otherwise.
	 */
	public boolean isEmpty() {
		for (E element : elements)
			if (element != null)
				return false;
		return true;
	}

	/**
	 * Check if the Stack is full.
	 * 
	 * @return true if the Stack is full, false otherwise.
	 */
	public boolean isFull() {
		for (E element : elements)
			if (element == null)
				return false;
		return true;
	}

	/**
	 * View the top-most element of the Stack without modifying it.
	 * 
	 * @return the top-most element of the Stack.
	 */
	public E peek() {
		if (isEmpty())
			return null;

		return elements[findTopIndex()];
	}

	/**
	 * Get the top-most element and remove it from the Stack.
	 * 
	 * @return the top-most element that was removed from the Stack.
	 */
	public E pop() {
		if (isEmpty())
			throw new EmptyStackException();

		E returnCache = elements[findTopIndex()];
		elements[findTopIndex()] = null;
		return returnCache;
	}

	/**
	 * Push an element into the Stack.
	 * 
	 * @param obj
	 *            - an element to be put into the Stack.
	 */
	public void push(E obj) {
		if (obj == null)
			throw new IllegalArgumentException();

		if (!isFull())
			elements[findTopIndex() + 1] = obj;
	}

	/**
	 * Calculate the size of the Stack (NOT the capacity).
	 * 
	 * @return size of the Stack.
	 */
	public int size() {
		return findTopIndex() + 1;
	}

	/**
	 * Find the index of the top-most element of the Stack.
	 * 
	 * @return index of the top-most element of the Stack.
	 */
	private int findTopIndex() {
		if (isFull()) {
			return capacity() - 1;
		}
		for (int i = 0; i < elements.length; i++)
			if (elements[i] == null)
				return i - 1;
		return -1;
	}
}
